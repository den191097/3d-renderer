package com.mobimore.Utils;

import com.mobimore.graphicsObjects.CoordsDrawable;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Random;

/**
 * Created by mobimore on 11/6/16.
 */
public class FileUtils {
    public static CoordsDrawable loadWithFileChooser(Component parent){
        JFileChooser fileChooser = new JFileChooser();
        CoordsDrawable coordsDrawable = null;
        int retValue = fileChooser.showOpenDialog(parent);
        if (retValue == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try {
                coordsDrawable = CoordsDrawable.fromObjFile(file, "loadedObject_"+new Random().nextInt());
                String objName = (String)JOptionPane.showInputDialog(
                        parent,
                        "Parsing file done, input object name:",
                        "Parsing done",
                        JOptionPane.PLAIN_MESSAGE,
                        null, //icon
                        null,
                        file.getName().substring(0, file.getName().lastIndexOf('.')));
                if ((objName != null) && (objName.length() > 0)) {
                    coordsDrawable.setName(objName);
                }else{
                    coordsDrawable = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Error parsing file", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        return coordsDrawable;
    }
}
