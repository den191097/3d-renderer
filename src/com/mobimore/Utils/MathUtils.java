package com.mobimore.Utils;

import Jama.Matrix;

/**
 * Created by mobimore on 11/15/16.
 */
public class MathUtils {

    public static double[][] multiplyMatrix(double[][] transformationArr, double[][] originalArr) {
        Matrix originalMatrix = new Matrix(originalArr).transpose();
        Matrix transformationMatrix = new Matrix(transformationArr);
        Matrix result = transformationMatrix.times(originalMatrix);
        return result.transpose().getArray();
    }

    public static double[][] multiplyMatrix(Matrix transformationMatrix, double[][] originalArr) {
        Matrix originalMatrix = new Matrix(originalArr).transpose();
        Matrix result = transformationMatrix.times(originalMatrix);
        return result.transpose().getArray();
    }

    /*public static float inverseLerp(float minVal, float maxVal, float lerpValue) {
        return (lerpValue - minVal) / (maxVal - minVal);
    }*/
}
