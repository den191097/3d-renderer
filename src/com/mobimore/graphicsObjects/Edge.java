
package com.mobimore.graphicsObjects;

/**
 * Created by mobimore on 11/9/16.
 */
public class Edge {
    private Vertex start, end;
    public double currX = 0, currY = 0/*, currZx = 0 /* not used? */, currZy = 0;
    private double calcCurrX = 0, calcCurrY = 0/*, calcCurrZx = 0*/, calcCurrZy = 0;
    private double dx = 0, dy = 0/*, dzX = 0*/, dzY = 0;

    public double currNx = 0, currNy = 0, currNzy = 0;
    public double calcCurrNx = 0, calcCurrNy = 0, calcCurrNzy = 0;
    private double Ndx = 0, Ndy = 0/*, dzX = 0*/, NdzY = 0;

    public Edge(Vertex start, Vertex end) {
        this.start = new Vertex(start);
        this.end = new Vertex(end);
        reset();
    }
    public Edge(Edge edge){
        this.start = new Vertex(edge.getStart());
        this.end = new Vertex(edge.getEnd());
        reset();
    }

    public void update(){
        calcCurrX += dx;
        //calcCurrZx += dzX;//for drawing scanline slices
        calcCurrZy += dzY;//for use with scanline
        calcCurrY += dy;
        currX = calcCurrX;// + 0.5;
        currY = calcCurrY;// + 0.5;
        //currZx = calcCurrZx;// + 0.5;
        currZy = calcCurrZy;// + 0.5;

        calcCurrNx += Ndx;
        //calcCurrZx += dzX;//for drawing scanline slices
        calcCurrNzy += NdzY;//for use with scanline
        calcCurrNy += Ndy;
        currNx = calcCurrNx;// + 0.5;
        currNy = calcCurrNy;// + 0.5;
        //currZx = calcCurrZx;// + 0.5;
        currNzy = calcCurrNzy;// + 0.5;
    }

    public Vertex getStart() {
        return start;
    }

    public void setStart(Vertex start) {
        this.start = new Vertex(start);
        reset();
    }

    public Vertex getEnd() {
        return end;
    }

    public void setEnd(Vertex end) {
        this.end = new Vertex(end);
        reset();
    }

    public void reset(){
        //Calculations for scanline renderer
        double lx = this.end.position.x - this.start.position.x;
        double ly = this.end.position.y - this.start.position.y;
        double lz = this.end.position.z - this.start.position.z;

        double Nlx = this.end.normal.x - this.start.normal.x;
        double Nly = this.end.normal.y - this.start.normal.y;
        double Nlz = this.end.normal.z - this.start.normal.z;

        dx = 0;
        dy = 0;
        dzY = 0;

        Ndx = 0;
        Ndy = 0;
        NdzY = 0;
        if(ly!=0){
            dx = lx / ly;
            dy = ly / ly;
            //dzX = lz / lx;
            dzY = lz / ly;

            Ndx = Nlx / ly;
            Ndy = Nly / ly;
            NdzY = Nlz / ly;
        }


        calcCurrX = this.start.position.x;// + 0.5 * Math.signum(dx);
        //calcCurrZx = this.start.z;// + 0.5 * Math.signum(dzX);
        calcCurrZy = this.start.position.z;// + 0.5 * Math.signum(dzY);
        calcCurrY = this.start.position.y;// + 0.5 * Math.signum(dy);
        currX = calcCurrX;// + 0.5;
        currY = calcCurrY;// + 0.5;
        //currZx = calcCurrZx;// + 0.5;
        currZy = calcCurrZy;// + 0.5;

        calcCurrNx = this.start.normal.x;// + 0.5 * Math.signum(dx);
        calcCurrNy = this.start.normal.y;// + 0.5 * Math.signum(dy);
        calcCurrNzy = this.start.normal.z;// + 0.5 * Math.signum(dzY);
        currNx = calcCurrNx;// + 0.5;
        currNy = calcCurrNy;// + 0.5;
        currNzy = calcCurrNzy;// + 0.5;
    }
}
