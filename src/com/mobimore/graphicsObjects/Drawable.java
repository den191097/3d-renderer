package com.mobimore.graphicsObjects;

import com.mobimore.light.LightSource;

import java.awt.*;

/**
 * Created by mobimore on 10/9/16.
 */
public abstract class Drawable {
    private boolean visible = true;
    private String name;

    public abstract void paint(final Graphics graphics, final Camera camera, final LightSource lightSource, final int[][] zBuffer);
    public final boolean isVisible() {
        return visible;
    }
    public final void setVisible(boolean visible) {
        this.visible = visible;
    }
    public abstract Vect3D getCenter();
    public final String getName() {
        return name;
    }
    public final void setName(String name) {
        this.name = name;
    }
}
