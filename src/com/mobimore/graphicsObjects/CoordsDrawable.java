package com.mobimore.graphicsObjects;

import Jama.Matrix;
import com.mobimore.SimpleObjParser;
import com.mobimore.Utils.MathUtils;
import com.mobimore.WavefrontObject;
import com.mobimore.light.LightSource;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static com.mobimore.Utils.MathUtils.multiplyMatrix;
import static java.lang.Math.*;

/**
 * Created by mobimore on 11/5/16.
 */
public class CoordsDrawable extends Drawable {
    private float Ka = 0.5f;
    private float Kd = 0.8f;
    private float Km = 0.9f;
    private double[][] coordinates;
    private int[][] polygons;
    private Vect3D[] vertexNormals;
    private int originVectPos = 0;
    private Color color = Color.WHITE;
    private Camera camera;
    private LightSource lightSource;

    public CoordsDrawable(double[][] coordinates, int[][] polygons, String name) {
        this.coordinates = coordinates;
        this.polygons = polygons;
        setName(name);
        originVectPos = findTopLeftCorner();
    }

    public static CoordsDrawable fromObjFile(File file, String name) throws IOException {
        SimpleObjParser simpleObjParser = new SimpleObjParser();
        simpleObjParser.parse(new FileReader((file)));
        WavefrontObject object = simpleObjParser.getWavefrontObjects().get(0);
        if (object == null) {
            throw new IOException();
        }
        double[][] coordinatesO = object.getCoordinates();
        double[][] coordinates = new double[coordinatesO.length][coordinatesO[0].length + 1];
        for (int i = 0; i < coordinates.length; i++) {
            System.arraycopy(coordinatesO[i], 0, coordinates[i], 0, coordinates[0].length - 1);
            coordinates[i][coordinates[0].length - 1] = 1;
        }
        /*double[][] coordinates = {
                {0,0,0, 1},
                {10,20,10,1},
                {20,0,0,1},
                {20, 0, 20,1},
                {20, 20, 10,1},
                {0,0,20,1}
        };*/
        int[][] polygons = object.getPolygons();
        return new CoordsDrawable(coordinates, polygons, name);
    }

    public void writeToObjFile(String name) throws IOException {
        File savedObjects = new File("savedObjects");
        if (savedObjects.isFile() || !savedObjects.exists()) {
            savedObjects.mkdir();
        }

        File file = new File(savedObjects, name);
        file.createNewFile();
        FileWriter fileWriter = new FileWriter(file, false);
        for (double[] vect : coordinates) {
            fileWriter.write("v " + Double.toString(vect[0]) + " " + Double.toString(vect[1]) + " " + Double.toString(vect[2]) + '\n');
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int[] polygon : polygons) {
            stringBuilder.delete(0, stringBuilder.length());
            stringBuilder.append("f ");
            for (int i = 0; i < polygon.length; i++) {
                stringBuilder.append(Integer.toString(polygon[i] + 1));
                if (i != polygon.length - 1) {
                    stringBuilder.append(" ");
                }
            }
            stringBuilder.append('\n');
            fileWriter.write(stringBuilder.toString());
        }
        fileWriter.close();
    }

    @Override
    public void paint(final Graphics graphics, final Camera camera, final LightSource lightSource, final int[][] zBuffer) {
        this.camera = camera;
        this.lightSource = lightSource;
        //long start = System.currentTimeMillis();
        BufferedImage renderedImage = rasterize(zBuffer);
        //long elapsedTimeMillis = System.currentTimeMillis()-start;
        //System.out.println("Rendered in: " + elapsedTimeMillis +" ms");
        graphics.drawImage(renderedImage, 0, 0, null);
        renderedImage.flush();
    }

    private BufferedImage rasterize(int[][] zBuffer) {
        computeVertexNormals();
        int[][] coordinatesT = toScreenCoordinates(perspectiveCoords(camera));
        BufferedImage image = new BufferedImage(zBuffer.length, zBuffer[0].length, BufferedImage.TYPE_4BYTE_ABGR);
        // /computeVertexNormals(coordinatesT);

        //Vect3D cameraPos = camera.getPosition();
        //long start = System.currentTimeMillis();
        for (int[] polygon1 : polygons) {
            Vertex[] polygon = new Vertex[polygon1.length];
            for (int i = 0; i < polygon1.length; i++) {
                polygon[i] = new Vertex(new Vect3D(coordinatesT[polygon1[i]][0], coordinatesT[polygon1[i]][1], coordinatesT[polygon1[i]][2]), vertexNormals[polygon1[i]]);
            }
            //Vect3D v1 = polygon[0].position;
            //Vect3D camToPtV = v1.minus(cameraPos);
            //double mult = camToPtV.dot(surfaceNormals[i1]);
            //if (mult < 0) {
            /*Random random = new Random();
            float r = random.nextFloat();
            float g = random.nextFloat();
            float b = random.nextFloat();
            color = new Color(r, g, b);*/
            fillPolygon(image, polygon, zBuffer, color);
            //}
        }
        //long elapsedTimeMillis = System.currentTimeMillis()-start;
        //System.out.println("Rendered in: " + elapsedTimeMillis +" ms");

        /*
        Vect3D[] polygon1 = new Vect3D[3];
        polygon1[0] = new Vect3D(coordinatesT[0][0], coordinatesT[0][1], coordinatesT[0][2]);
        polygon1[1] = new Vect3D(coordinatesT[1][0], coordinatesT[1][1], coordinatesT[1][2]);
        polygon1[2] = new Vect3D(coordinatesT[2][0], coordinatesT[2][1], coordinatesT[2][2]);

        Vect3D[] polygon2 = new Vect3D[3];
        polygon2[0] = new Vect3D(coordinatesT[2][0], coordinatesT[2][1], coordinatesT[2][2]);
        polygon2[1] = new Vect3D(coordinatesT[1][0], coordinatesT[1][1], coordinatesT[1][2]);
        polygon2[2] = new Vect3D(coordinatesT[3][0], coordinatesT[3][1], coordinatesT[3][2]);

        Vect3D[] polygon3 = new Vect3D[3];
        polygon3[0] = new Vect3D(coordinatesT[3][0], coordinatesT[3][1], coordinatesT[3][2]);
        polygon3[1] = new Vect3D(coordinatesT[1][0], coordinatesT[1][1], coordinatesT[1][2]);
        polygon3[2] = new Vect3D(coordinatesT[5][0], coordinatesT[5][1], coordinatesT[5][2]);

        Vect3D[] polygon4 = new Vect3D[3];
        polygon4[0] = new Vect3D(coordinatesT[5][0], coordinatesT[5][1], coordinatesT[5][2]);
        polygon4[1] = new Vect3D(coordinatesT[1][0], coordinatesT[1][1], coordinatesT[1][2]);
        polygon4[2] = new Vect3D(coordinatesT[0][0], coordinatesT[0][1], coordinatesT[0][2]);

        fillPolygon(image, polygon1, zBuffer, Color.RED);
        fillPolygon(image, polygon2, zBuffer, Color.GREEN);
        fillPolygon(image, polygon3, zBuffer, Color.BLUE);
        fillPolygon(image, polygon4, zBuffer, Color.YELLOW);
        */

        //Debug triangle
        /*Vect3D[] polygon = new Vect3D[3];
        polygon[0] = new Vect3D(40, 40, 10);
        polygon[1] = new Vect3D(100, 100, 0);
        polygon[2] = new Vect3D(120, 60, 0);
        fillPolygon(coordinatesT, image, polygon, zBuffer, color2);*/

        return image;
    }

    private void fillPolygon(BufferedImage image, Vertex[] polygon, int[][] zBuffer, Color color) {
        if (polygon.length < 3) return;
        Vect3D cameraPosition = camera.getPosition();
        Vect3D lightSourcePosition = lightSource.getPosition();
        float Ip = lightSource.getStrength();
        double eZ = cameraPosition.z;
        int zMin = (int)-eZ, zMax = (int) eZ; //From perspective
        float[] hsbVals = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
        Edge[] edges = new Edge[polygon.length];
            for (int i = 0; i < polygon.length - 1; i++) {
                edges[i] = new Edge(polygon[i], polygon[i + 1]);
            }
        edges[polygon.length-1] = new Edge(polygon[polygon.length-1], polygon[0]);

        for (Edge edge : edges) {
            if (edge.getStart().position.y > edge.getEnd().position.y) {
                Vertex temp = edge.getStart();
                edge.setStart(edge.getEnd());
                edge.setEnd(temp);
            }
        }

        for (int i = 0; i < edges.length - 1; i++) {
            for (int j = 0; j < edges.length - 1; j++) {
                if (edges[j].getStart().position.y > edges[j + 1].getStart().position.y) {
                    // swap both edges
                    Edge temp = edges[j];
                    edges[j] = edges[j + 1];
                    edges[j + 1] = temp;
                }
            }
        }

        int scanlineEnd = (int) edges[edges.length-1].getEnd().position.y;

        // maybe unneeded
        for (Vertex vertex : polygon) {
            Vect3D point3D = vertex.position;
            if (scanlineEnd < (int) point3D.y)
                scanlineEnd = (int) point3D.y;
        }

        scanlineEnd = Math.min(scanlineEnd, zBuffer[0].length);

        int scanline = (int) edges[0].getStart().position.y;

        ArrayList<Edge> activeEdges = computeActiveEdges(edges, scanline);

        while (scanline < scanlineEnd){
            if(activeEdges.isEmpty()) break;

            if(scanline == (int) activeEdges.get(0).getEnd().position.y || scanline == (int) activeEdges.get(1).getEnd().position.y){
                activeEdges = computeActiveEdges(edges, scanline);
            }

            if (scanline < 0){
                activeEdges.get(0).update();
                activeEdges.get(1).update();
                scanline++;
                continue;
            }

            Vect3D start = new Vect3D((int) activeEdges.get(0).currX, scanline, (int) activeEdges.get(0).currZy);
            Vect3D end = new Vect3D((int) activeEdges.get(1).currX, scanline, (int) activeEdges.get(1).currZy);

            Vect3D Nstart = new Vect3D(activeEdges.get(0).currNx, activeEdges.get(0).currNy, activeEdges.get(0).currNzy);
            Vect3D Nend = new Vect3D(activeEdges.get(1).currNx, activeEdges.get(1).currNy, activeEdges.get(1).currNzy);

            //System.out.println("NORMAL Y ON X: [1]: " + activeEdges.get(0).currNy + " [2]: " + activeEdges.get(1).currNy);

            if (end.x < start.x) {
                Vect3D temp = start;
                start = end;
                end = temp;

                temp = Nstart;
                Nstart = Nend;
                Nend = temp;
            }

            double dzX = 0;
            double NdxX = 0;//change in normal x while moving through x
            double NdyX = 0;//change in normal y while moving through x
            double NdzX = 0;//change in normal z while moving through x
            if ((int) start.x != (int) end.x) {
                dzX = (end.z - start.z) / ((int) end.x - (int) start.x);
                NdzX = (Nend.z - Nstart.z) / ((int) end.x - (int) start.x);
                NdxX = (Nend.x - Nstart.x) / ((int) end.x - (int) start.x);
                NdyX = (Nend.y - Nstart.y) / ((int) end.x - (int) start.x);
            }

            double currZx = start.z;
            double currNxX = Nstart.x;
            //noinspection SuspiciousNameCombination
            double currNyX = Nstart.y;
            double currNzX = Nstart.z;
            /*System.out.println("CURRNzX: " + currNzX);
            System.out.println("lx: "+((int) end.x - (int) start.x));
            System.out.println("NdzX: " + NdzX);
            System.out.println("StartX, EndX: "+start.x+ " "+end.x);*/
            for (int x = (int) start.x; x < (int) end.x; x++) {
                if (x >= 0 && x < zBuffer.length && (int) currZx < zMax && (int) currZx > zMin) {
                    if (zBuffer[x][scanline] < (int) currZx) {
                        Vect3D pixelPosition = new Vect3D(x, scanline, (int) currZx);
                        Vect3D toCamera = (cameraPosition.minus(pixelPosition)).normal();
                        Vect3D toLight = (lightSourcePosition.minus(pixelPosition));
                        float toLightSquareDistance = (float) pow(toLight.length()/10,2);
                        toLight.normalize();
                        float pointFactor = 1 / (1.0f + toLightSquareDistance);
                        //System.out.println("DISTANCE: " + (lightSourcePosition.minus(pixelPosition)).length());
                        //System.out.println("POINT FACTOR: "+pointFactor);
                        //noinspection SuspiciousNameCombination
                        Vect3D vertexNormal = new Vect3D(currNxX, currNyX, currNzX).normal();
                        Vect3D reflectanceVector = (vertexNormal.mult(toLight.dot(vertexNormal) * 2).minus(toLight)).normal();
                        float globalIntens = Ip * Ka;
                        float diffuseIntens = Ip * Kd * (float) max(toLight.dot(vertexNormal), 0);
                        float reflectIntens = Ip * Km * (float) pow(max(reflectanceVector.dot(toCamera), 0), 100);
                        float luminancea = pointFactor*(globalIntens + (diffuseIntens + reflectIntens));
                        luminancea = min(1, luminancea);
                        //float luminancea = MathUtils.inverseLerp(zMin, zMax, (int) currZx);
                        hsbVals[2] = luminancea;
                        Color color1 = Color.getHSBColor(hsbVals[0], hsbVals[1], hsbVals[2]);
                        image.setRGB(x, scanline, color1.getRGB());
                        zBuffer[x][scanline] = (int) (currZx);
                    }
                }
                currZx += dzX;
                currNzX += NdzX;
                currNyX += NdyX;
                currNxX += NdxX;
            }
            //System.out.println("CURRNzX_End: " + currNzX);
            //System.out.println("End: " + Nend.z);
            activeEdges.get(0).update();
            activeEdges.get(1).update();

            scanline++;
        }

        /*for (Edge edge : edges) {
           image.getGraphics().drawLine((int) edge.getStart().x, (int) edge.getStart().y, (int) edge.getEnd().x, (int) edge.getEnd().y);
        }*/


    }

    private ArrayList<Edge> computeActiveEdges(Edge[] edges, int scanline){
        ArrayList<Edge> activeEdges = new ArrayList<>();
        for (Edge edge : edges) {
            if (scanline >= edge.getStart().position.y && scanline < edge.getEnd().position.y) {
                activeEdges.add(edge);
            }
        }
        return activeEdges;
    }

    public final synchronized void translate(double dx, double dy, double dz) {
        coordinates = multiplyMatrix(translationMatrix(dx, dy, dz), coordinates);
    }

    public final synchronized void vectRotate(double angle, Vect3D vect, boolean inPlace) {
        //Vect3D vect1 = new Vect3D(0, 0, 0);
        if (vect.getX() < 0) {
            vect = new Vect3D(0 - vect.x, 0 - vect.y, 0 - vect.z);
        }
        Matrix translate = new Matrix(new double[][]{
                {1.0, 0.0, 0.0, 0.0},
                {0.0, 1.0, 0.0, 0.0},
                {0.0, 0.0, 1.0, 0.0},
                {0.0, 0.0, 0.0, 1.0}
        });
        if (inPlace) {
            translate = translationMatrix(-coordinates[originVectPos][0], -coordinates[originVectPos][1], -coordinates[originVectPos][2]);
        }
        double sinY = vect.z / sqrt(pow(vect.x, 2) + pow(vect.y, 2) + pow(vect.z, 2));
        Matrix rotateY = rotateYMatrix(sinY);
        double sinZ = vect.y / sqrt(pow(vect.x, 2) + pow(vect.y, 2) + pow(vect.z, 2));
        Matrix rotateZ = rotateZMatrix(sinZ);
        double angleRad = Math.toRadians(angle);
        Matrix rotateX = rotateXMatrix(sin(angleRad));
        Matrix fullTransform = translate.inverse().times(rotateY.inverse()).times(rotateZ.inverse()).times(rotateX)
                .times(rotateZ).times(rotateY).times(translate);
        coordinates = MathUtils.multiplyMatrix(fullTransform.getArray(), coordinates);
    }

    public final synchronized void scale(double w, double h, double s) {
        Matrix translate = translationMatrix(-coordinates[originVectPos][0], -coordinates[originVectPos][1], -coordinates[originVectPos][2]);
        Matrix transformation = new Matrix(new double[][]{
                {w, 0, 0, 0},
                {0, h, 0, 0},
                {0, 0, s, 0},
                {0, 0, 0, 1}
        });
        Matrix fullTransformation = translate.inverse().times(transformation).times(translate);
        coordinates = multiplyMatrix(fullTransformation, coordinates); //Scale
    }

    private static Matrix translationMatrix(double dx, double dy, double dz) {
        double transformation[][] = {
                {1.0, 0.0, 0.0, dx},
                {0.0, 1.0, 0.0, dy},
                {0.0, 0.0, 1.0, dz},
                {0.0, 0.0, 0.0, 1.0}
        };
        return new Matrix(transformation);
    }

    private static Matrix rotateXMatrix(double sinA) {
        double cosA = sqrt(1 - pow(sinA, 2));
        double transformation[][] = {
                {1.0, 0.0, 0.0, 0.0},
                {0.0, cosA, -sinA, 0.0},
                {0.0, sinA, cosA, 0.0},
                {0.0, 0.0, 0.0, 1.0}
        };
        return new Matrix(transformation);
    }

    private static Matrix rotateYMatrix(double sinA) {
        double cosA = sqrt(1 - pow(sinA, 2));
        double transformation[][] = {
                {cosA, 0.0, sinA, 0.0},
                {0.0, 1.0, 0.0, 0.0},
                {-sinA, 0, cosA, 0.0},
                {0.0, 0.0, 0.0, 1.0}
        };
        return new Matrix(transformation);
    }

    private static Matrix rotateZMatrix(double sinA) {
        double cosA = sqrt(1 - pow(sinA, 2));
        double transformation[][] = {
                {cosA, sinA, 0.0, 0.0},
                {-sinA, cosA, 0.0, 0.0},
                {0.0, 0.0, 1.0, 0.0},
                {0.0, 0.0, 0.0, 1.0}
        };
        return new Matrix(transformation);
    }

    @Override
    public Vect3D getCenter() {
        double xSum = 0, ySum = 0, zSum = 0;
        for (double coords[] : coordinates) {
            xSum += coords[0];
            ySum += coords[1];
            zSum += coords[2];
        }
        return new Vect3D(xSum / coordinates.length, ySum / coordinates.length, zSum / coordinates.length);
    }

    public double getHeight() {
        double yMin = coordinates[0][1];
        double yMax = coordinates[0][1];
        for (double[] coordinate : coordinates) {
            if (yMin > coordinate[1]) yMin = coordinate[1];
            if (yMax < coordinate[1]) yMax = coordinate[1];
        }
        return Math.abs(yMax - yMin);
    }

    public double getWidth() {
        double xMin = coordinates[0][0];
        double xMax = coordinates[0][0];
        for (double[] coordinate : coordinates) {
            if (xMin > coordinate[0]) xMin = coordinate[0];
            if (xMax < coordinate[0]) xMax = coordinate[0];
        }
        return Math.abs(xMax - xMin);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    private static int[][] toScreenCoordinates(double[][] coordinates) {
        int[][] screenCoordinates = new int[coordinates.length][coordinates[0].length];
        for (int i = 0; i < coordinates.length; i++) {
            for (int j = 0; j < coordinates[0].length; j++) {
                screenCoordinates[i][j] = (int) (coordinates[i][j] / coordinates[i][3]);
            }
        }
        return screenCoordinates;
    }

    private double[][] perspectiveCoords(final Camera camera) {
        return multiplyMatrix(camera.getPerspectiveArr(), coordinates);
    }

    private int findTopLeftCorner() {
        int[] xArr = new int[coordinates.length];
        int[] yArr = new int[coordinates.length];
        int[] zArr = new int[coordinates.length];
        for (int i = 0; i < xArr.length; i++) {
            xArr[i] = (int) coordinates[i][0];
            yArr[i] = (int) coordinates[i][1];
            zArr[i] = (int) coordinates[i][2];
        }
        int minX, minY, minZ;
        minX = findMinValue(xArr);
        minY = findMinValue(yArr);
        minZ = findMinValue(zArr);

        int topLeftCornerVectPos = 0;
        for (int i = 0; i < coordinates.length; i++) {
            if ((int) coordinates[i][0] == minX && (int) coordinates[i][1] == minY && (int) coordinates[i][2] == minZ) {
                topLeftCornerVectPos = i;
                break;
            }
        }
        return topLeftCornerVectPos;
    }

    private static int findMinValue(int[] arr) {
        int minValue = arr[0];
        for (int i = 0; i < arr.length - 1; i++) {
            if (minValue > arr[i]) minValue = arr[i];
        }
        return minValue;
    }

    private Vect3D[] computeSurfaceNormals(){
        int[][] coordsT = toScreenCoordinates(coordinates);
        Vect3D[] surfaceNormals = new Vect3D[polygons.length];
        for (int i = 0, polygonsLength = polygons.length; i < polygonsLength; i++) {
            int[] polygon = polygons[i];
            int x = coordsT[polygon[1]][0] - coordsT[polygon[0]][0];
            int y = coordsT[polygon[1]][1] - coordsT[polygon[0]][1];
            int z = coordsT[polygon[1]][2] - coordsT[polygon[0]][2];
            Vect3D v1 = new Vect3D(x, y, z);

            x = coordsT[polygon[2]][0] - coordsT[polygon[1]][0];
            y = coordsT[polygon[2]][1] - coordsT[polygon[1]][1];
            z = coordsT[polygon[2]][2] - coordsT[polygon[1]][2];
            Vect3D v2 = new Vect3D(x, y, z);

            Vect3D vN = v1.cross(v2);
            vN.normalize(); //!!!!
            surfaceNormals[i] = vN;

            //System.out.println("Normal [" + i + "]: " + vN);
        }
        return surfaceNormals;
    }

    private void computeVertexNormals(){
        Vect3D[] surfaceNormals = computeSurfaceNormals();
        vertexNormals = new Vect3D[coordinates.length];
        for (int currVertNumber = 0; currVertNumber < coordinates.length; currVertNumber++) {
            Vect3D normSums = new Vect3D(0, 0, 0);
            int count = 0;
            for (int i = 0, polygonsLength = polygons.length; i < polygonsLength; i++) {
                int[] polygon = polygons[i];
                for (int verticleNum : polygon) {
                    if (verticleNum == currVertNumber) {
                        normSums = normSums.plus(surfaceNormals[i]);
                        count++;
                    }
                }
            }
            if (count != 0) {
                Vect3D vN = normSums.div(count);
                vN.normalize();
                vertexNormals[currVertNumber] = vN;
                //System.out.println("Normal [" + i1 + "]: " + vN);
            }
        }
    }
}

 /*private void computeVertexNormals(int[][] coordsT){
        java.util.List<Vect3D[]> list = new ArrayList<>();
        for (int[] polygon1 : polygons) {
            Vect3D[] polygon = new Vect3D[polygon1.length];
            for (int i = 0; i < polygon1.length; i++) {
                polygon[i] = new Vect3D(coordsT[polygon1[i]][0], coordsT[polygon1[i]][1], coordsT[polygon1[i]][2]);
            }
            list.add(polygon);
        }
        vertexNormals = new Vect3D[coordsT.length];
        for (int i1 = 0; i1 < coordsT.length; i1++) {
            int[] verticle = coordsT[i1];
            Vect3D coordVert = new Vect3D(verticle);
            Vect3D normSums = new Vect3D(0, 0, 0);
            int count = 0;
            for (int i = 0; i < list.size(); i++) {
                Vect3D[] polyVects = list.get(i);
                for (Vect3D vect3D : polyVects) {
                    if (vect3D.equals(coordVert)) {
                        normSums = normSums.plus(surfaceNormals[i]);
                        count++;
                    }
                }
            }
            if (count != 0) {
                Vect3D vN = normSums.div(count);
                vN.normalize();
                vertexNormals[i1] = vN;
                //System.out.println("Normal [" + i1 + "]: " + vN);
            }
        }
    }*/

//JOBJ Parser
/*JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(file));
        JSONArray coordinatesJArr = (JSONArray) jsonObject.get("coordinates");

        coordinates = new double[coordinatesJArr.size()][((JSONArray) coordinatesJArr.get(0)).size()+1];

        for(int i=0;i<coordinates.length;i++) {
            for (int j = 0; j < coordinates[0].length-1; j++) {
                //TODO Test this
                Object val = ((JSONArray) coordinatesJArr.get(i)).get(j);
                if(val.getClass()==Double.class){
                    coordinates[i][j]=(double) val;
                }else{
                    if (val.getClass() == Long.class) coordinates[i][j]=((Long) val).doubleValue();
                }
            }
            coordinates[i][coordinates[0].length-1]=1;
        }

        JSONArray edgesJArr = (JSONArray) jsonObject.get("edges");
        edges = new int[edgesJArr.size()][((JSONArray) edgesJArr.get(0)).size()];

        for(int i=0;i<edges.length;i++) {
            for (int j = 0; j < edges[0].length; j++) {
                edges[i][j]= ((Long) ((JSONArray) edgesJArr.get(i)).get(j)).intValue();
            }
        }*/

//Write to JOBJ
/*File file = new File(savedObjects, name);
        file.createNewFile();
                FileWriter fileWriter = new FileWriter(file, false);

                JSONObject jsonObject = new JSONObject();
                double[][] screenCoordinates = toScreenCoordinates(coordinates);

                JSONArray coordinatesJArr = new JSONArray();
                for (double vector[] : screenCoordinates) {
                JSONArray jsonVector = new JSONArray();
                for (int i = 0; i < vector.length - 1; i++) {
        double coordinate = vector[i];
        jsonVector.add(coordinate);
        }
        coordinatesJArr.add(jsonVector);
        }
        jsonObject.put("coordinates", coordinatesJArr);

        JSONArray edgesJArr = new JSONArray();
        for (int edge[] : this.edges) {
        JSONArray jsonEdge = new JSONArray();
        for (int point : edge) {
        jsonEdge.add(point);
        }
        edgesJArr.add(jsonEdge);
        }
        jsonObject.put("edges", edgesJArr);
        jsonObject.writeJSONString(fileWriter);
        fileWriter.close();
*/