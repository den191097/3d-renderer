package com.mobimore.graphicsObjects;

/**
 * Created by mobimore on 1/2/17.
 */
public class Vertex {
    public Vect3D position;
    public Vect3D normal;

    public Vertex(Vect3D position, Vect3D normal) {
        this.position = new Vect3D(position);
        this.normal = new Vect3D(normal);
    }

    public Vertex(Vertex vertex){
        position = new Vect3D(vertex.position);
        normal = new Vect3D(vertex.normal);
    }
}
