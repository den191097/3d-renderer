package com.mobimore.GTest;

import com.mobimore.Utils.FileUtils;
import com.mobimore.graphicsObjects.Camera;
import com.mobimore.graphicsObjects.CoordsDrawable;
import com.mobimore.graphicsObjects.Vect3D;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by mobimore on 9/8/16.
 */
class Main implements DrawingPanel.DrawablesListener {
    private enum States {
        MOVE, ROTATE, SCALE
    }

    private JPanel mainPanel,drawPanel;
    private final JFrame frame;
    private JLabel modeLabel;
    private JFormattedTextField lineXTextField,lineYTextField,lineZTextField;
    private JCheckBox rotateInPlaceCheckBox;
    private JList<String> objectsList;
    private JButton addButton,removeButton,saveButton,resetButton;
    private JTextField lPosXTextField;
    private JTextField lPosYTextField;
    private JTextField lPosZTextField;
    private JSlider strengthSlider;
    private CoordsDrawable selectedObj;
    private boolean optMode;
    private Vect3D vect;
    private States state = States.MOVE;
    private Camera camera;
    private DefaultListModel<String> listModel;
    private String selectedObjName;
    private final String defaultURL = "objects" + File.separatorChar + "sourceLetterO.obj";
    private final String defaultObjName = "letterO";

    private Main() {
        frame = new JFrame("Main");
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        int width = gd.getDisplayMode().getWidth() / 2;
        int height = gd.getDisplayMode().getHeight() / 2;
        drawPanel.setPreferredSize(new Dimension(width, height));
        drawPanel.setFocusable(true);

        frame.pack();
        frame.setVisible(true);

        Vect3D cameraPosition = new Vect3D(drawPanel.getWidth() / 2, drawPanel.getHeight() / 2, 200);
        camera = new Camera(cameraPosition, new Vect3D(0, 0, 0));
        ((DrawingPanel) drawPanel).setCamera(camera);
        Vect3D lightPosition = cameraPosition.minus(new Vect3D(0, 0, 50));
        ((DrawingPanel) drawPanel).getLightSource().setPosition(lightPosition);
        ((DrawingPanel) drawPanel).getLightSource().setStrength(100f);
        drawPanel.requestFocus();

        lPosXTextField.setText(Double.toString(lightPosition.x));
        lPosYTextField.setText(Double.toString(lightPosition.y));
        lPosZTextField.setText(Double.toString(lightPosition.z));
        strengthSlider.setValue((int) (((DrawingPanel) drawPanel).getLightSource().getStrength()));
        initDefaultObjects();

        drawPanel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                //super.keyPressed(e); //With this produces runtime undefined behavior
                if (selectedObj == null) return;
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_RIGHT:
                        switch (state) {
                            case MOVE:
                                if (!optMode) selectedObj.translate(10, 0, 0);
                                break;
                            case ROTATE:
                                selectedObj.vectRotate(4, vect, rotateInPlaceCheckBox.isSelected());
                                break;
                            case SCALE:
                                if (!optMode) selectedObj.scale(1.1, 1, 1);
                                break;
                        }
                        drawPanel.repaint();
                        break;
                    case KeyEvent.VK_LEFT:
                        switch (state) {
                            case MOVE:
                                if (!optMode) selectedObj.translate(-10, 0, 0);
                                break;
                            case ROTATE:
                                selectedObj.vectRotate(-4, vect, rotateInPlaceCheckBox.isSelected());
                                break;
                            case SCALE:
                                if (!optMode) selectedObj.scale(1 / 1.1, 1, 1);
                                break;
                        }
                        drawPanel.repaint();
                        break;
                    case KeyEvent.VK_UP:
                        switch (state) {
                            case MOVE:
                                if (optMode) {
                                    selectedObj.translate(0, 0, -10);
                                } else {
                                    selectedObj.translate(0, -10, 0);
                                }
                                break;
                            case SCALE:
                                if (optMode) {
                                    selectedObj.scale(1, 1, 1.1);
                                } else {
                                    selectedObj.scale(1, 1.1, 1);
                                }
                                break;
                        }
                        drawPanel.repaint();
                        break;

                    case KeyEvent.VK_DOWN:
                        switch (state) {
                            case MOVE:
                                if (optMode) {
                                    selectedObj.translate(0, 0, 10);
                                } else {
                                    selectedObj.translate(0, 10, 0);
                                }
                                break;
                            case SCALE:
                                if (optMode) {
                                    selectedObj.scale(1, 1, 1 / 1.1);
                                } else {
                                    selectedObj.scale(1, 1 / 1.1, 1);
                                }
                                break;
                        }
                        drawPanel.repaint();
                        break;

                    case KeyEvent.VK_R:
                        state = States.ROTATE;
                        modeLabel.setText("Rotating");
                        break;
                    case KeyEvent.VK_S:
                        state = States.SCALE;
                        modeLabel.setText("Scaling XY");
                        break;
                    case KeyEvent.VK_ALT:
                        optMode = true;
                        if (state == States.SCALE) {
                            modeLabel.setText("Scaling Z");
                        } else {
                            modeLabel.setText("Moving Z");
                        }
                        break;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_R:
                        state = States.MOVE;
                        modeLabel.setText("Moving XY");
                        break;
                    case KeyEvent.VK_S:
                        state = States.MOVE;
                        modeLabel.setText("Moving XY");
                        break;
                    case KeyEvent.VK_ALT:
                        optMode = false;
                        if (state == States.SCALE) {
                            modeLabel.setText("Scaling XY");
                        } else {
                            modeLabel.setText("Moving XY");
                        }
                        break;
                }
            }
        });
        resetButton.addActionListener(e -> {
            //TODO fix reset button
            ((DrawingPanel) drawPanel).removeDrawable(defaultObjName);
            try {
                selectedObj = CoordsDrawable.fromObjFile(new File(defaultURL), selectedObj.getName());
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(null, "Error reading file", "Error", JOptionPane.ERROR_MESSAGE);
            }
            selectedObj.setColor(new Color(255, 0, 0, 127));
            //letterO.scale(scaleF,scaleF,scaleF);
            ((DrawingPanel) drawPanel).addDrawable(selectedObj);
            drawPanel.repaint();
            drawPanel.requestFocus();
        });

        FocusAdapter vectPosTextEditAdapter = new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                updateVectPosFromUI();
                drawPanel.repaint();
            }
        };
        lineXTextField.addFocusListener(vectPosTextEditAdapter);
        lineYTextField.addFocusListener(vectPosTextEditAdapter);
        lineZTextField.addFocusListener(vectPosTextEditAdapter);

        FocusAdapter lightPosTextEditAdapter = new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                updateLightPosFromUI();
                drawPanel.repaint();
            }
        };

        lPosXTextField.addFocusListener(lightPosTextEditAdapter);
        lPosYTextField.addFocusListener(lightPosTextEditAdapter);
        lPosZTextField.addFocusListener(lightPosTextEditAdapter);
        strengthSlider.addChangeListener(e -> {
            ((DrawingPanel) drawPanel).getLightSource().setStrength(strengthSlider.getValue()/*/100f*255f*/);
            drawPanel.repaint();
        });

        saveButton.addActionListener(e -> {
            try {
                selectedObj.writeToObjFile(selectedObj.getName() + ".obj");
                drawPanel.repaint();
                drawPanel.requestFocus();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        objectsList.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                selectedObjName = objectsList.getSelectedValue();
                selectedObj = (CoordsDrawable) ((DrawingPanel) drawPanel).getDrawable(selectedObjName);
            }
        });

        MouseAdapter mouseAdapter = new MouseAdapter() {
            int dragStartX = 0, dragStartY = 0;

            @Override
            public void mousePressed(MouseEvent e) {
                drawPanel.requestFocus();
                dragStartX = e.getX();
                dragStartY = e.getY();
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                drawPanel.requestFocus();
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                int dx = e.getX() - dragStartX;
                int dy = e.getY() - dragStartY;
                switch (state) {
                    case MOVE:
                        if (optMode) {
                            selectedObj.translate(0, 0, dy / 2);
                        } else {
                            selectedObj.translate(dx, dy, 0);
                        }
                        break;
                }
                drawPanel.repaint();
                dragStartX = e.getX();
                dragStartY = e.getY();
            }
        };

        drawPanel.addMouseMotionListener(mouseAdapter);
        drawPanel.addMouseListener(mouseAdapter);
        addButton.addActionListener(e -> {
            CoordsDrawable coordsDrawable = FileUtils.loadWithFileChooser(frame);
            if (coordsDrawable != null) {
                if (!listModel.contains(coordsDrawable.getName())) {
                    coordsDrawable.setColor(new Color(255, 0, 0, 255));
                    ((DrawingPanel) drawPanel).addDrawable(coordsDrawable);
                } else {
                    JOptionPane.showMessageDialog(null, "Object with this name exists", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            drawPanel.requestFocus();
        });
        removeButton.addActionListener(e -> {
            ((DrawingPanel) drawPanel).removeDrawable(selectedObjName); //onObjectRemoved is called
            drawPanel.requestFocus();
        });
    }

    private void initDefaultObjects() {
        CoordsDrawable letterO;
        try {
            File file = new File(defaultURL);
            letterO = CoordsDrawable.fromObjFile(file, defaultObjName);
            letterO.setColor(new Color(0, 0, 255, 127));
            ((DrawingPanel) drawPanel).addDrawable(letterO);
            //double scaleF=0.25*drawPanel.getHeight()/letterO.getHeight();
            //letterO.scale(scaleF,scaleF,scaleF);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Can't load default object\nAdd objects manually", "Warning", JOptionPane.WARNING_MESSAGE);
        }

        vect = new Vect3D(100, 100, 0);
        Line3D line3D = new Line3D(new Vect3D(0, 0, 0), vect, "line3D");
        line3D.setColor(Color.BLUE);

        ((DrawingPanel) drawPanel).addDrawable(line3D);
    }

    private void createUIComponents() {
        drawPanel = new DrawingPanel(this);
        listModel = new DefaultListModel<>();
        objectsList = new JList<>(listModel);
        objectsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public static void main(String[] args) {
        System.setProperty("sun.java2d.opengl", "true"); //hardware acceleration
        SwingUtilities.invokeLater(Main::new);
    }

    @Override
    public void onObjectAdded(final String objectName) {
        listModel.addElement(objectName);
        selectedObjName = objectName;
        objectsList.setSelectedValue(objectName, true);
    }

    @Override
    public void onObjectRemoved(final String objectName) {
        int index = objectsList.getSelectedIndex();
        listModel.remove(listModel.indexOf(objectName));
        int size = listModel.getSize();
        selectedObjName = "";
        selectedObj = null;
        if (size != 0) {
            if (index == size) index--;
            objectsList.setSelectedIndex(index);
            objectsList.ensureIndexIsVisible(index);
        }
        System.gc();

    }

    @Override
    public void onPanelCleared() {
        listModel.clear();
        selectedObjName = "";
        selectedObj = null;
        System.gc();
    }

    private void updateLightPosFromUI(){
        Vect3D prevPos = ((DrawingPanel) drawPanel).getLightSource().getPosition();
        try {
            prevPos.x = Double.parseDouble(lPosXTextField.getText());
        } catch (NumberFormatException e) {
            lPosXTextField.setText(Double.toString(prevPos.x));
        }
        try {
            prevPos.y = Double.parseDouble(lPosYTextField.getText());
        } catch (NumberFormatException e) {
            lPosYTextField.setText(Double.toString(prevPos.y));
        }
        try {
            prevPos.z = Double.parseDouble(lPosZTextField.getText());
        } catch (NumberFormatException e) {
            lPosZTextField.setText(Double.toString(prevPos.z));
        }
        ((DrawingPanel) drawPanel).getLightSource().setPosition(prevPos);
    }
    private void updateVectPosFromUI(){
        try {
            vect.x = Double.parseDouble(lineXTextField.getText());
        } catch (NumberFormatException e) {
            lineXTextField.setText(Double.toString(vect.x));
        }
        try {
            vect.y = Double.parseDouble(lineYTextField.getText());
        } catch (NumberFormatException e) {
            lineYTextField.setText(Double.toString(vect.y));
        }
        try {
            vect.z = Double.parseDouble(lineZTextField.getText());
        } catch (NumberFormatException e) {
            lineZTextField.setText(Double.toString(vect.z));
        }
    }
}
