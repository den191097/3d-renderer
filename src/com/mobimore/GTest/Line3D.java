package com.mobimore.GTest;

import com.mobimore.graphicsObjects.Camera;
import com.mobimore.graphicsObjects.Drawable;
import com.mobimore.graphicsObjects.Vect3D;
import com.mobimore.light.LightSource;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Created by mobimore on 10/9/16.
 */
public class Line3D extends Drawable {

    private Vect3D lineStart;
    private Vect3D lineEnd;
    private Color color = Color.WHITE;

    public Line3D(Vect3D lineStart, Vect3D lineEnd, String name) {
        this.lineStart = lineStart;
        this.lineEnd = lineEnd;
        setName(name);
    }
    public Line3D(double x1, double y1, double z1, double x2, double y2, double z2, String name) {
        lineStart = new Vect3D(x1, y1, z1);
        lineEnd = new Vect3D(x2, y2, z2);
        setName(name);
    }

    public Vect3D getLineStart() {
        return lineStart;
    }
    public void setLineStart(Vect3D lineStart) {
        this.lineStart = lineStart;
    }
    public Vect3D getLineEnd() {
        return lineEnd;
    }
    public void setLineEnd(Vect3D lineEnd) {
        this.lineEnd = lineEnd;
    }
    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void paint(final Graphics graphics, final Camera camera, final LightSource lightSource, int[][] zBuffer) {

        Color prevColor = graphics.getColor();
        graphics.setColor(color);

        Point2D.Double projectS = new Point2D.Double(lineStart.getX()+ camera.getPerspectiveArr()[0][3], lineStart.getY()+ camera.getPerspectiveArr()[1][3]);///(1-lineStart.getZ()), lineStart.getY()/(1-lineStart.getZ()));
        Point2D.Double projectE = new Point2D.Double(lineEnd.getX()+ camera.getPerspectiveArr()[0][3], lineEnd.getY()+ camera.getPerspectiveArr()[1][3]);///(1-lineEnd.getZ()), lineEnd.getY()/(1-lineEnd.getZ()));
        /*
        double[][] matrix = {{lineStart.getX(), lineStart.getY(), lineStart.getZ(), 1},
                             {lineEnd.getX(), lineEnd.getY(), lineEnd.getZ(), 1}};

        double[][] matrixP = MathUtils.multiplyMatrix(camera.getPerspectiveArr(), matrix);

        projectS = new Point2D.Double(matrixP[0][0]/matrixP[0][3], matrixP[0][1]/matrixP[0][3]);
        projectE = new Point2D.Double(matrixP[1][0]/matrixP[1][3], matrixP[1][1]/matrixP[0][3]);
        */
        Line2D line2D = new Line2D.Double(projectS, projectE);
        ((Graphics2D) graphics).draw(line2D);

        graphics.setColor(prevColor);

    }

    @Override
    public Vect3D getCenter() {
        return null;
    }
}
