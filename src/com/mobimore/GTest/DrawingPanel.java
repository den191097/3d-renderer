package com.mobimore.GTest;

import com.mobimore.graphicsObjects.Camera;
import com.mobimore.graphicsObjects.CoordsDrawable;
import com.mobimore.graphicsObjects.Drawable;
import com.mobimore.light.LightSource;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mobimore on 10/9/16.
 */
class DrawingPanel extends JPanel {
    private final Map<String, Drawable> drawableMap = new HashMap<>();
    private DrawablesListener drawablesListener;
    private Camera camera;
    private LightSource lightSource;
    private int[][] zBuffer;
    //private BufferedImage image;

    public DrawingPanel(Camera camera, DrawablesListener drawablesListener) {
        this.camera = camera;
        this.drawablesListener = drawablesListener;
        camera.setDx(getWidth() / 2);
        camera.setDy(getHeight() / 2);
        lightSource = new LightSource(camera.getPosition(), 1.0f);
    }
    public DrawingPanel(Camera camera) {
        this.camera = camera;
        camera.setDx(getWidth() / 2);
        camera.setDy(getHeight() / 2);
        lightSource = new LightSource(camera.getPosition(), 1.0f);
    }

    public DrawingPanel(DrawablesListener drawablesListener) {
        this.camera = new Camera(0, 0, 0, getWidth()/2, getHeight()/2, 0);
        lightSource = new LightSource(camera.getPosition(), 1.0f);
        this.drawablesListener = drawablesListener;
    }
    public DrawingPanel() {
        this.camera = new Camera(0, 0, 0, getWidth()/2, getHeight()/2, 0);
        lightSource = new LightSource(camera.getPosition(), 1.0f);
    }
    public void setDrawablesListener(DrawablesListener drawablesListener) {
        this.drawablesListener = drawablesListener;
    }

    @Override
    protected void paintComponent(Graphics g) {
        //super.paintComponent(g);
        int width = getWidth();
        int height = getHeight();
        initZBuffer(width, height);
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, width, height);

        g.setColor(Color.WHITE);
        g.drawLine(-width, height/2, width, height/2);
        g.drawLine(width/2, -height, width/2, height);
        g.drawOval((int) lightSource.getPosition().x-5, (int) lightSource.getPosition().y-5, 10, 10);

        for (Map.Entry<String, Drawable> entry : drawableMap.entrySet()) {
            Drawable drawable = entry.getValue();
            if (drawable.isVisible()) drawable.paint(g, camera, lightSource, zBuffer);
        }
    }

    public Camera getCamera() {
        return camera;
    }
    public void setCamera(Camera camera) {
        this.camera = camera;
        camera.setDx(getWidth() / 2);
        camera.setDy(getHeight() / 2);
        repaint();
    }

    public boolean addDrawable(Drawable drawable) {
        String name = drawable.getName();
        if (drawableMap.containsKey(name)) {
            return false;
        }
        drawableMap.put(name, drawable);
        if (drawablesListener != null) {
            //TODO fix Drawable objects and CoordsDrawable objects
            if(drawable.getClass() == CoordsDrawable.class) SwingUtilities.invokeLater(() -> drawablesListener.onObjectAdded(name));
        }
        repaint();
        return true;
    }
    public Drawable getDrawable(String name){
        return drawableMap.get(name);
    }
    public boolean removeDrawable(String name){
        if (!drawableMap.containsKey(name)) {
            return false;
        }
        drawableMap.remove(name);
        if (drawablesListener != null) {
            SwingUtilities.invokeLater(() -> drawablesListener.onObjectRemoved(name));
        }
        repaint();
        return true;
    }
    public void clearPanel() {
        //drawables.clear();
        drawableMap.clear();
        if (drawablesListener != null) {
            SwingUtilities.invokeLater(() -> drawablesListener.onPanelCleared());
        }
        repaint();
    }

    public String[] getDrawableNames(){
        return drawableMap.keySet().toArray(new String[0]);
    }

    public interface DrawablesListener{
        void onObjectAdded(final String objectName);
        void onObjectRemoved(final String objectName);
        void onPanelCleared();
    }

    private void initZBuffer(int width, int height){
        zBuffer = new int[width][height];
        for (int i = 0; i < zBuffer.length; i++) {
            for (int j = 0; j < zBuffer[0].length; j++) {
                zBuffer[i][j] = -200;
            }
        }
    }

    public LightSource getLightSource() {
        return lightSource;
    }
}
